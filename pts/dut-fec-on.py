#!/usr/bin/env python
################################################################################
## CERN BE-CO-HT
## CONV-RS485-OPT-RTM
## https://www.ohwr.org/projects/conv-rs485-opt-rtm
################################################################################
##
## unit name:   dut-fec-on.py
##
## description: Script to switch ON the DUT FEC ELMA crate
##
################################################################################
## Copyright CERN 2019
################################################################################
##
## This program is free software: you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation, either version 3 of the License, or (at your option) any later
## version.
##
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
## FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
##
## You should have received a copy of the GNU General Public License along with
## this program. If not, see <https://www.gnu.org/licenses/>.
##
## SPDX-License-Identifier: GPL-3.0-or-later
################################################################################

import os
import sys
import time
import configparser

config = configparser.ConfigParser()
config.read('CONFIG.ini')

ip   = config['DUT.FEC']['MgtHostName']
pwrd = config['DUT.FEC']['MgtPassword']

# Turn on DUT.FEC
fd  = os.popen("snmpset -v2c -c %s %s 1.3.6.1.4.1.37968.1.1.7.2.1.3.1 i 0" % (pwrd, ip))
ret = fd.close()
if (ret != None):
    print "ERROR accessing DUT.FEC over SNMP, exiting..."
    sys.exit(ret >> 8)
time.sleep(2)

print
print "---------------"
print
print "DUT FEC cfvm-774-opt-pts2 is ON now"
