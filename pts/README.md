# CONV-RS485-OPT-RTM Production Test System (PTS)

This is the software for the the CONV-RS485-OPT-RTM PTS.

To configure it, please edit directly the file `CONFIG.ini`. In particular, please make sure that
you have set `CpuUserName` to an account name you have access to on the Front-End Computer (FEC)
that is hosting the Central Timing Receiver (CTR) cards.

To run it, simply run the `run-pts.py` python script.

**Dependencies:**
- snmp
- python configparser
