#!/usr/bin/env python
################################################################################
## CERN BE-CO-HT
## CONV-RS485-OPT-RTM
## https://www.ohwr.org/projects/conv-rs485-opt-rtm
################################################################################
##
## unit name:   test02.py
##
## description: Test all transmitters and receivers by feeding them with a GMT
## signal and verifying that there are no errors on the CTRs.
##
################################################################################
## Copyright CERN 2019
################################################################################
##
## This program is free software: you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation, either version 3 of the License, or (at your option) any later
## version.
##
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
## FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
##
## You should have received a copy of the GNU General Public License along with
## this program. If not, see <https://www.gnu.org/licenses/>.
##
## SPDX-License-Identifier: GPL-3.0-or-later
################################################################################

###TODO: it seems the test passes when there is no CTR in the crate:-o

import os
import string
import time
import sys
import configparser

sys.path.append('..')
import utilities as util
import ptsexcept


def get_ctrtest_err(user, rsa, host, port, module):

    cmd = 'echo "mo {0} rst" | /usr/local/bin/ctrtest-timdt | grep -A 7 -e MsMissedErrs | xargs'.format(module)
    output = os.popen("ssh -i %s -p %d %s@%s '%s'" % (rsa, port, user, host, cmd)).read()

    items  = string.split(output)

    mserr   = '{0} {1} {2}'.format(items[1][1:],  items[3][1:],  items[4])
    pllerr  = '{0} {1} {2}'.format(items[6][1:],  items[8][1:],  items[9])
    misserr = '{0} {1} {2}'.format(items[11][1:], items[13][1:], items[14])

    return mserr, pllerr, misserr



def main (card=None, default_directory='.',suite=None, serial=""):

    testname= "Test02: GMT transmission / reception"
    util.header_msg( testname, [ "GMT transmission / reception"] )

    ###############################################################################
    ############################ initialization ###################################
    ###############################################################################

    config = configparser.ConfigParser()
    config.read(default_directory + "/../../CONFIG.ini")

    host = config['CTR.FEC']['CpuHostName']
    port = int(config['CTR.FEC']['CpuPort'])
    user = config['CTR.FEC']['CpuUserName']
    rsa = config['CTR.FEC']['CpuUserNameRsa']

    duration = float(config['CTR.FEC']['TstDuration'])

    ref_ms1   = 0
    ref_pll1  = 0
    ref_miss1 = 0

    ref_ms2   = 0
    ref_pll2  = 0
    ref_miss2 = 0

    ms1   = [0, 0, 0, 0]
    pll1  = [0, 0, 0, 0]
    miss1 = [0, 0, 0, 0]

    ms2   = [0, 0, 0, 0]
    pll2  = [0, 0, 0, 0]
    miss2 = [0, 0, 0, 0]

    errors = False

    test_results={}

    test_results['PLL LEDs'] = 1
    test_results['Check CTR MS'] = 1
    test_results['Check CTR PLL'] = 1
    test_results['Check CTR frames'] = 1

    ###############################################################################
    ############################ actual test ######################################
    ###############################################################################

    if not util.ask_user("Are the PLL and TMG LEDs on all four CTR cards ON?"):
        test_results['PLL LEDs'] = 0
        util.err_msg("Some of the optical transmitters/receivers might be broken")
    else:
        util.user_msg("Test running, please wait..") 
        done = 0

        time.sleep (110) # needed for stabilisation of Module 4; otherwise Ms errors
        while (not done):
            # get error status from reference CTR
            ref_ms1, ref_pll1, ref_miss1 =  get_ctrtest_err(user, rsa, host, port, 5)

            # get error status from DUT CTRs
            for i in range(0,4):
                ms1[i], pll1[i], miss1[i] =  get_ctrtest_err(user, rsa, host, port, i+1)

            # sleep for some time, waiting for errors (not) to happen
            time.sleep(duration)

            # get error status from DUT CTRs
            for i in range(0,4):
                ms2[i], pll2[i], miss2[i] =  get_ctrtest_err(user, rsa, host, port, i+1)

            # get error status from reference CTR
            ref_ms2, ref_pll2, ref_miss2 =  get_ctrtest_err(user, rsa, host, port, 5)

            # check for errors on reference CTR
            if ((ref_ms1 != ref_ms2) or (ref_pll1 != ref_pll2) or (ref_miss1 != ref_miss2)):
                util.user_msg("WARNING: timing errors detected on reference CTR, repeating test...")
            else:
                done = 1

        # Check for errors on DUT CTRs
        for i in range(0,4):

            if (ms1[i] != ms2[i]):
                test_results['Check CTR MS'] = 0
                util.err_msg("Module %s: Ms errors detected ({%s} != {%s})" %
                             (i+1, ms2[i], ms1[i]))

            if (pll1[i] != pll2[i]):
                test_results['Check CTR PLL'] = 0
                util.err_msg("Module %s: PLL errors detected ({%s} != {%s})" %
                             (i+1, pll2[i], pll1[i]))

            if (miss1[i] != miss2[i]):
                test_results['Check CTR frames'] = 0
                util.err_msg("Module %s: Missed errors detected ({%s} != {%s})" %
                             (i+1, miss2[i], miss1[i]))

    ###############################################################################
    ########################## result processing ##################################
    ###############################################################################

    errors = util.summarise_test_results(testname, test_results)
    sys.stdout.flush()
    return 0

if __name__ == '__main__' :
    main()
