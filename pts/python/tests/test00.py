#!/usr/bin/env python
################################################################################
## CERN BE-CO-HT
## CONV-RS485-OPT-RTM
## https://www.ohwr.org/projects/conv-rs485-opt-rtm
################################################################################
##
## unit name:   test00.py
##
## description: Test for reading out the RTM ID over I2C
##
################################################################################
## Copyright CERN 2019
################################################################################
##
## This program is free software: you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation, either version 3 of the License, or (at your option) any later
## version.
##
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
## FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
##
## You should have received a copy of the GNU General Public License along with
## this program. If not, see <https://www.gnu.org/licenses/>.
##
## SPDX-License-Identifier: GPL-3.0-or-later
################################################################################

# Import system modules
import os
import sys
import time
import configparser

# Import common modules
sys.path.append('..')
import utilities as util
import vv_skt

def main (card=None, default_directory='.',suite=None, serial=""):

    testname= "Test00: DUT identification"
    util.header_msg( testname, ["DUT identification"] )

    ###############################################################################
    ############################ initialization ###################################
    ###############################################################################

    util.section_msg("I2C to read RTM ID")
    test_results={}

    config = configparser.ConfigParser()
    config.read(default_directory + "/../../CONFIG.ini")

    lun  = int(config['DUT.FEC']['Slot'])
    ip   = config['DUT.FEC']['MgtHostName']
    port = int(config['DUT.FEC']['MgtPort'])
    user = config['DUT.FEC']['MgtUserName']
    pwrd = config['DUT.FEC']['MgtPassword']

    dut = vv_skt.SKT(lun, ip, port, user, pwrd)

    ###############################################################################
    ############################ actual test ######################################
    ###############################################################################
    rtm = (dut.vv_read(4) >> 16) & 0x3f
    if (rtm == 0x05):
        test_results['DUT identification']= 1
        util.info_msg("DUT identified; detection lines read correctly: 0x%02X" % rtm)
    else:
        test_results['DUT identification']= 0
        util.err_msg("Identified wrong DUT; expected RTM ID 0x05 and received 0x%02X" % rtm)

    ###############################################################################
    ########################## result processing ##################################
    ###############################################################################

    errors = util.summarise_test_results(testname, test_results)
    sys.stdout.flush()
    return 0;

if __name__ == '__main__' :
    main()
