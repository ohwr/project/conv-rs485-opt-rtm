#!/usr/bin/env python
################################################################################
## CERN BE-CO-HT
## CONV-RS485-OPT-RTM
## https://www.ohwr.org/projects/conv-rs485-opt-rtm
################################################################################
##
## unit name:   test01.py
##
## description: Test LEDs
##
################################################################################
## Copyright CERN 2019
################################################################################
##
## This program is free software: you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation, either version 3 of the License, or (at your option) any later
## version.
##
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
## FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
##
## You should have received a copy of the GNU General Public License along with
## this program. If not, see <https://www.gnu.org/licenses/>.
##
## SPDX-License-Identifier: GPL-3.0-or-later
################################################################################

import sys
import os
import string
import time

sys.path.append('..')
import utilities as util
import ptsexcept


def test_led_all():

    util.section_msg("Testing DUT LEDs OFF state")

    if not util.ask_user("Are all the LEDs of the DUT OFF now?"):
        util.err_msg("Some LEDs might be broken")
        return 0

    util.section_msg("Testing DUT LEDs ON state")
    util.user_msg("---> Please clean all fibers and connectors, make all the front panel connections and press [ENTER]")
    ret = raw_input("")
    # switch ON VME crate now..
        # loop vv_read of BIDR reg to check when initialization is completed

    if not util.ask_user("Are all the LEDs of the DUT ON now?"):
        util.err_msg("Some LEDs or other board components might be broken")
        return 0

    util.info_msg("All LEDs verified successfully!")
    return 1



def main (card=None, default_directory='.',suite=None, serial=""):

    testname= "Test01: LEDs and basic connectivity"
    util.header_msg( testname, [ "LED connectivity"] )

    ###############################################################################
    ############################ initialization ###################################
    ###############################################################################
    test_results={}

    ###############################################################################
    ############################ actual test ######################################
    ###############################################################################
    test_results['LEDs'] = test_led_all()

    ###############################################################################
    ########################## result processing ##################################
    ###############################################################################
    errors = util.summarise_test_results( testname, test_results)

    sys.stdout.flush()
    return 0;

if __name__ == '__main__' :
    main()
