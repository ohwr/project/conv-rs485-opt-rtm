#!/usr/bin/env python
################################################################################
## CERN BE-CO-HT
## CONV-RS485-OPT-RTM
## https://www.ohwr.org/projects/conv-rs485-opt-rtm
################################################################################
##
## unit name:  vv_skt.py
##
## description: A simple python class to handle Telnet communication with an
## ELMA crate.
##
################################################################################
## Copyright CERN 2014-2019
################################################################################
##
## This program is free software: you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation, either version 3 of the License, or (at your option) any later
## version.
##
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
## FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
##
## You should have received a copy of the GNU General Public License along with
## this program. If not, see <https://www.gnu.org/licenses/>.
##
## SPDX-License-Identifier: GPL-3.0-or-later
################################################################################

import socket

from ptsexcept import *
import utilities as util

class SKT:

    def __init__(self, lun, ip, port, user, pwrd):
        """ Telnet access over a socket to ELMA I2C bus
        """
        self.lun  = lun
        self.ip   = ip
        self.port = port
        self.user = user
        self.pwrd = pwrd

        addr = (self.ip, self.port)
        s = socket.create_connection(addr)
        s.recv(256)
        s.send("%s\r\n" % self.user)
        s.recv(256)
        s.send("%s\r\n" % self.pwrd)
        s.recv(256)
        self.handle = s

        # get crate firmware version, to apply proper address in readreg/writereg
        self.handle.send("version\r\n")
        ver = self.handle.recv(256)
        pos = ver.find("Software version")
        if (pos == -1):
            util.crit_msg("Crate %s not responding as expected" % self.addr)
            sys.exit(2)
        ver = float(ver[pos+17:pos+21])
        self.ver = ver

    def vv_read(self, byte_offset):
        """ Read from the application FPGA via ELMA telnet
            The byte offset will be aligned to D32
            The value will contain the 32 bit integer read
        """

        try:
            cm = "readreg %d %x\r\n" % (self.lun, byte_offset)
            if (self.ver < 2.27):
                rn = byte_offset/4 + 1
                cm = "readreg %d %d\r\n" % (self.lun,rn)
            self.handle.send(cm)
            orig = self.handle.recv(256)
            rp = orig
            rp = rp.split(" ")[3]
            rp = rp.split("\n")[0]
            rp = int(rp,16)

        except Exception as e:
            msg = "vv_read: No reply from register at address 0x%03x " % (byte_offset)
            raise PtsException(msg)

        return rp

    def vv_close(self):
        """ Close the socket
        """
        self.handle.shutdown(SHUT_RDWR)
        self.handle.close()
        self.handle = 0
        return 0
