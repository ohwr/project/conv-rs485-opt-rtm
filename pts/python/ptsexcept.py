#!/usr/bin/env python
################################################################################
## CERN BE-CO-HT
## CONV-RS485-OPT-RTM
## https://www.ohwr.org/projects/conv-rs485-opt-rtm
################################################################################
##
## unit name:   ptsexcept.py
##
## description: Generic PTS framework used for running the various tests and
## logging their outputs: common exceptions and handling
##
################################################################################
## Copyright CERN 2011-2019
################################################################################
##
## This program is free software: you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation, either version 3 of the License, or (at your option) any later
## version.
##
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
## FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
##
## You should have received a copy of the GNU General Public License along with
## this program. If not, see <https://www.gnu.org/licenses/>.
##
## SPDX-License-Identifier: GPL-3.0-or-later
################################################################################

class PtsException(Exception):
    pass

class PtsCritical(PtsException):
    """critical error, abort the whole test suite"""
    pass

class PtsError(PtsException):
    """error, continue remaining tests in test suite"""
    pass

class PtsUser(PtsException):
    """error, user intervention required"""
    pass

class PtsWarning(PtsException):
    """warning, a cautionary message should be displayed"""
    pass

class PtsInvalid(PtsException):
    """reserved: invalid parameters"""

class PtsNoBatch(PtsInvalid):
    """reserved: a suite was created without batch of tests to run"""
    pass

class PtsBadTestNo(PtsInvalid):
    """reserved: a bad test number was given"""
    pass

if __name__ == '__main__':
    pass
