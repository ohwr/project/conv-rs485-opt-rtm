#!/usr/bin/env python
################################################################################
## CERN BE-CO-HT
## CONV-RS485-OPT-RTM
## https://www.ohwr.org/projects/conv-rs485-opt-rtm
################################################################################
##
## unit name:   run-pts.py
##
## description: Top level script to run the full PTS of the CONV-RS485-OPT-RTM.
##
################################################################################
## Copyright CERN 2019
################################################################################
##
## This program is free software: you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation, either version 3 of the License, or (at your option) any later
## version.
##
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
## FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
##
## You should have received a copy of the GNU General Public License along with
## this program. If not, see <https://www.gnu.org/licenses/>.
##
## SPDX-License-Identifier: GPL-3.0-or-later
################################################################################

import os
import subprocess
import sys
import errno
import time
import configparser

BOARD  = "OptRtm"
LOGDIR = os.path.abspath('log')

config = configparser.ConfigParser()
config.read('CONFIG.ini')

ip   = config['DUT.FEC']['MgtHostName']
pwrd = config['DUT.FEC']['MgtPassword']

# Turn off DUT.FEC
fd  = os.popen("snmpset -v2c -c %s %s 1.3.6.1.4.1.37968.1.1.7.2.1.3.1 i 1" % (pwrd, ip))
ret = fd.close()
if (ret != None):
    print "ERROR accessing DUT.FEC over SNMP, exiting..."
    sys.exit(ret >> 8)
time.sleep(2)

try:
    os.makedirs(LOGDIR)
except OSError as exc:
    if exc.errno == errno.EEXIST and os.path.isdir(LOGDIR):
        pass
    else:
        raise

serial = "0000"

if (len(sys.argv) > 1):
    serial = sys.argv[1]
else:
    serial = raw_input(
        "Please scan CERN serial number bar-code, then press [ENTER]: ")

if (serial == ""):
    serial = "0000"

if (len(sys.argv) > 2):
    extra_serial = sys.argv[2]
else:
    extra_serial = raw_input(
        "If needed input extra serial number and press [ENTER] OR just press [ENTER]: ")

if (extra_serial == ""):
    extra_serial = "0000"

print

board_plugged = raw_input("---> Now please plug in the board and then press [ENTER]")

# Turn on DUT.FEC
fd  = os.popen("snmpset -v2c -c %s %s 1.3.6.1.4.1.37968.1.1.7.2.1.3.1 i 0" % (pwrd, ip))
ret = fd.close()
if (ret != None):
    print "ERROR accessing DUT.FEC over SNMP, exiting..."
    sys.exit(ret >> 8)
time.sleep(10)

# run tests
os.chdir('python')
cmd  = 'python pts.py -b {0} -s {1} -e {2} -l {3} 00 01 02'.format(
    BOARD, serial, extra_serial, LOGDIR)
subprocess.call(cmd, shell = True)
os.chdir('..')

# Turn off DUT.FEC
fd  = os.popen("snmpset -v2c -c %s %s 1.3.6.1.4.1.37968.1.1.7.2.1.3.1 i 1" % (pwrd, ip))
ret = fd.close()
if (ret != None):
    print "ERROR accessing DUT.FEC over SNMP, exiting..."
    sys.exit(ret >> 8)
time.sleep(2)

print
print "---------------"
print
print "End of the test"
print

raw_input("Press [ENTER] to close this window..")
