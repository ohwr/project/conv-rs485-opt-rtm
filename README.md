# CONV-RS485-OPT-RTM

This is the repository for the the CONV-RS485-OPT-RTM.

Currently it contains the [software for the Production Test System software (PTS)](pts/), under the
`pts` directory.

For a new CONV-RS485-OPT-RTM PTS installation, check the setup guide [here](https://www.ohwr.org/project/conv-rs485-opt-rtm/wikis/pts-first-time-setup).